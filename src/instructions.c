//
// Created by Janek on 14.11.2020.
//
//TODO: empty references when getting something from SDA?
//TODO: possibly sum up error detection in external methods
//TODO: refine error messages to better fit

#include <stdio.h>
#include <stdlib.h>
#include "instructions.h"
#include "defines.h"
#include "stack.h"
#include "input.h"
#include "execution.h"
#include "types.h"
#include <bigint.h>
#include "memory.h"

ObjRef rvr = NULL;

ObjRef newCompoundObject(int numObjRefs) {
    ObjRef compObj;
    int objSize = sizeof(int) + sizeof(char) /*+ sizeof(ObjRef*)*/ + numObjRefs*sizeof(void *);
    //round up to multiples of 4
    //if(objSize%4>0) objSize = ((objSize/4)*4)+4;
    compObj = alloc(objSize);
    compObj->size = MSB|numObjRefs;
    compObj->brokenHeart = 0;
    for(int i = 0; i<numObjRefs; i++) {
        ((ObjRef *)compObj->data)[i] = NULL;
    }
    return compObj;
}

void instHALT(void) {
    //printf("\nFinished program execution.\n");
    if(gcstats!=0) garbageCollector();
}

void instPUSHC(int x) {
    bigFromInt(x);
    pushObj(bip.res);
}

void instADD(void) {
    StackSlot n2 = pop();
    StackSlot n1 = pop();
    if(!(n1.isObjRef && n2.isObjRef)) {
        fprintf(stderr, "ERROR: n1 or n2 is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n2.u.objRef==NULL||n1.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!(IS_PRIMITIVE(n2.u.objRef)&&IS_PRIMITIVE(n1.u.objRef))) {
        fprintf(stderr, "Error: Non-primitive object in arithmetic operation\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n1.u.objRef;
    bip.op2 = n2.u.objRef;
    bigAdd();
    pushObj(bip.res);
}

void instSUB(void) {
    StackSlot n2 = pop();
    StackSlot n1 = pop();
    if(!(n1.isObjRef && n2.isObjRef)) {
        fprintf(stderr, "ERROR: n1 or n2 is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n2.u.objRef==NULL||n1.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!(IS_PRIMITIVE(n2.u.objRef)&&IS_PRIMITIVE(n1.u.objRef))) {
        fprintf(stderr, "Error: Non-primitive object in arithmetic operation\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n1.u.objRef;
    bip.op2 = n2.u.objRef;
    bigSub();
    pushObj(bip.res);
}

void instMUL(void) {
    StackSlot n2 = pop();
    StackSlot n1 = pop();
    if(!(n1.isObjRef && n2.isObjRef)) {
        fprintf(stderr, "ERROR: n1 or n2 is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n2.u.objRef==NULL||n1.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!(IS_PRIMITIVE(n2.u.objRef)&&IS_PRIMITIVE(n1.u.objRef))) {
        fprintf(stderr, "Error: Non-primitive object in arithmetic operation\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n1.u.objRef;
    bip.op2 = n2.u.objRef;
    bigMul();
    pushObj(bip.res);
}

void instDIV(void) {
    StackSlot n2 = pop();
    StackSlot n1 = pop();
    if(!(n1.isObjRef && n2.isObjRef)) {
        fprintf(stderr, "ERROR: n1 or n2 is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n2.u.objRef==NULL||n1.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!(IS_PRIMITIVE(n2.u.objRef)&&IS_PRIMITIVE(n1.u.objRef))) {
        fprintf(stderr, "Error: Non-primitive object in arithmetic operation\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n1.u.objRef;
    bip.op2 = n2.u.objRef;
    bigDiv();
    pushObj(bip.res);
}

void instMOD(void) {
    StackSlot n2 = pop();
    StackSlot n1 = pop();
    if(!(n1.isObjRef && n2.isObjRef)) {
        fprintf(stderr, "ERROR: n1 or n2 is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n2.u.objRef==NULL||n1.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!(IS_PRIMITIVE(n2.u.objRef)&&IS_PRIMITIVE(n1.u.objRef))) {
        fprintf(stderr, "Error: Non-primitive object in arithmetic operation\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n1.u.objRef;
    bip.op2 = n2.u.objRef;
    bigDiv();
    pushObj(bip.rem);
}

void instRDINT(void) {
    bigRead(stdin);
    pushObj(bip.res);
}

void instWRINT(void) {
    StackSlot n = pop();
    if(!n.isObjRef) {
        fprintf(stderr, "ERROR: n is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!IS_PRIMITIVE(n.u.objRef)) {
        fprintf(stderr, "Error: Object is not a primitive object\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n.u.objRef;
    //printf("writing: %d\n", bigToInt());
    bigPrint(stdout);
}

void instRDCHR(void) {
    char input;
    scanf(" %c", &input);
    //char inputBuffer[15];
    //fgets(inputBuffer, sizeof(inputBuffer), stdin);
    //sscanf(inputBuffer, "%c", &input);
    bigFromInt((int) input);
    pushObj(bip.res);
}

void instWRCHR(void) {
    StackSlot n = pop();
    if(!n.isObjRef) {
        fprintf(stderr, "ERROR: n is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!IS_PRIMITIVE(n.u.objRef)) {
        fprintf(stderr, "Error: Object is not a primitive object\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n.u.objRef;
    printf("%c", bigToInt());
}

void instPUSHG(int x) {
    if(x>=sdaSize || x < 0) {
        fprintf(stderr, "Invalid position %d in static data area", x);
        exit(EXIT_FAILURE);
    }
    pushObj(*(sdaPtr+x));
}

void instPOPG(int x) {
    if(x>=sdaSize || x < 0) {
        fprintf(stderr, "Invalid position %d in static data area", x);
        exit(EXIT_FAILURE);
    }
    StackSlot n = pop();
    if(!n.isObjRef) {
        fprintf(stderr, "Stackslot doesnt hold ObjRef");
        exit(EXIT_FAILURE);
    }
    *(sdaPtr+x) = n.u.objRef;
}

void instASF(int x) {
    // Push frame pointer to stack
    pushInt(fp);
    fp = sp;
    if(fp + x >= (stackSize*1024)/sizeof(StackSlot)) {
        fprintf(stderr, "Error: Stack overflow on stack frame allocation\n");
        exit(EXIT_FAILURE);
    }
    sp = fp + x;
    //when creating space of local variables, they are all initialized with NULL
    for(int i = fp; i<sp; i++) {
        StackSlot s;
        s.isObjRef = true;
        s.u.objRef = NULL;
        stack[i] = s;
    }
}

void instRSF(void) {
    sp = fp;
    StackSlot n = pop();
    if(n.isObjRef) {
        fprintf(stderr, "Error: Unexpected ObjRef instead of FP on stack");
        exit(EXIT_FAILURE);
    }
    fp = n.u.number;
}

void instPUSHL(int x) {
    if(!stack[fp+x].isObjRef) {
        fprintf(stderr, "Error: Local variable to be pushed didnt contain object");
        exit(EXIT_FAILURE);
    }
    pushObj(stack[fp+x].u.objRef);
}

void instPOPL(int x) {
    stack[fp+x] = pop();
}


void instEQ(void) {
    StackSlot n2 = pop();
    StackSlot n1 = pop();
    if(!(n1.isObjRef && n2.isObjRef)) {
        fprintf(stderr, "ERROR: n1 or n2 is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n2.u.objRef==NULL||n1.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!(IS_PRIMITIVE(n2.u.objRef)&&IS_PRIMITIVE(n1.u.objRef))) {
        fprintf(stderr, "Error: Non-primitive object in arithmetic comparison\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n1.u.objRef;
    bip.op2 = n2.u.objRef;
    if(bigCmp() == 0) bigFromInt(1);
    else bigFromInt(0);
    pushObj(bip.res);
}

void instNE(void) {
    StackSlot n2 = pop();
    StackSlot n1 = pop();
    if(!(n1.isObjRef && n2.isObjRef)) {
        fprintf(stderr, "ERROR: n1 or n2 is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n2.u.objRef==NULL||n1.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!(IS_PRIMITIVE(n2.u.objRef)&&IS_PRIMITIVE(n1.u.objRef))) {
        fprintf(stderr, "Error: Non-primitive object in arithmetic comparison\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n1.u.objRef;
    bip.op2 = n2.u.objRef;
    if(bigCmp() != 0) bigFromInt(1);
    else bigFromInt(0);
    pushObj(bip.res);
}

void instLT(void){
    StackSlot n2 = pop();
    StackSlot n1 = pop();
    if(!(n1.isObjRef && n2.isObjRef)) {
        fprintf(stderr, "ERROR: n1 or n2 is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n2.u.objRef==NULL||n1.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!(IS_PRIMITIVE(n2.u.objRef)&&IS_PRIMITIVE(n1.u.objRef))) {
        fprintf(stderr, "Error: Non-primitive object in arithmetic comparison\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n1.u.objRef;
    bip.op2 = n2.u.objRef;
    if(bigCmp() < 0) bigFromInt(1);
    else bigFromInt(0);
    pushObj(bip.res);
}

void instLE(void) {
    StackSlot n2 = pop();
    StackSlot n1 = pop();
    if(!(n1.isObjRef && n2.isObjRef)) {
        fprintf(stderr, "ERROR: n1 or n2 is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n2.u.objRef==NULL||n1.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!(IS_PRIMITIVE(n2.u.objRef)&&IS_PRIMITIVE(n1.u.objRef))) {
        fprintf(stderr, "Error: Non-primitive object in arithmetic comparison\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n1.u.objRef;
    bip.op2 = n2.u.objRef;
    if(bigCmp() <= 0) bigFromInt(1);
    else bigFromInt(0);
    pushObj(bip.res);
}

void instGT(void) {
    StackSlot n2 = pop();
    StackSlot n1 = pop();
    if(!(n1.isObjRef && n2.isObjRef)) {
        fprintf(stderr, "ERROR: n1 or n2 is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n2.u.objRef==NULL||n1.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!(IS_PRIMITIVE(n2.u.objRef)&&IS_PRIMITIVE(n1.u.objRef))) {
        fprintf(stderr, "Error: Non-primitive object in arithmetic comparison\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n1.u.objRef;
    bip.op2 = n2.u.objRef;
    if(bigCmp() > 0) bigFromInt(1);
    else bigFromInt(0);
    pushObj(bip.res);
}

void instGE(void) {
    StackSlot n2 = pop();
    StackSlot n1 = pop();
    if(!(n1.isObjRef && n2.isObjRef)) {
        fprintf(stderr, "ERROR: n1 or n2 is not an object.");
        exit(EXIT_FAILURE);
    }
    if(n2.u.objRef==NULL||n1.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!(IS_PRIMITIVE(n2.u.objRef)&&IS_PRIMITIVE(n1.u.objRef))) {
        fprintf(stderr, "Error: Non-primitive object in arithmetic comparison\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n1.u.objRef;
    bip.op2 = n2.u.objRef;
    if(bigCmp() >= 0) bigFromInt(1);
    else bigFromInt(0);
    pushObj(bip.res);
}

void instJMP(int target) {
    if(target>=instCount) {
        fprintf(stderr, "Error: Jump target is higher than amount of instructions");
        exit(EXIT_FAILURE);
    }
    pc = target;
}

void instBRF(int target) {
    if(target>=instCount) {
        fprintf(stderr, "Error: Jump target is higher than amount of instructions");
        exit(EXIT_FAILURE);
    }
    StackSlot n = pop();
    if(!n.isObjRef) {
        fprintf(stderr, "ERROR: n is not an int object.");
        exit(EXIT_FAILURE);
    }
    if(n.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!IS_PRIMITIVE(n.u.objRef)) {
        fprintf(stderr, "Error: Object is not a primitive object\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n.u.objRef;
    int condition = bigToInt();
    if(!(condition==0||condition==1)) {
        fprintf(stderr, "Error: Condition for instruction BRF is not a boolean value");
        exit(EXIT_FAILURE);
    }
    if(condition==0) pc=target;
}

void instBRT(int target) {
    if(target>=instCount) {
        fprintf(stderr, "Error: Jump target is higher than amount of instructions");
        exit(EXIT_FAILURE);
    }
    StackSlot n = pop();
    if(!n.isObjRef) {
        fprintf(stderr, "ERROR: n is not an int object.");
        exit(EXIT_FAILURE);
    }
    if(n.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!IS_PRIMITIVE(n.u.objRef)) {
        fprintf(stderr, "Error: Object is not a primitive object\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n.u.objRef;
    int condition = bigToInt();
    if(!(condition==0||condition==1)) {
        fprintf(stderr, "Error: Condition for instruction BRF is not a boolean value");
        exit(EXIT_FAILURE);
    }
    if(condition==1) pc=target;
}

void instCALL(int target) {
    if(target>=instCount) {
        fprintf(stderr, "Error: Jump target for CALL is higher than amount of instructions");
        exit(EXIT_FAILURE);
    }
    pushInt(pc);
    pc = target;
}

void instRET(void) {
    StackSlot n = pop();
    if(n.isObjRef) {
        fprintf(stderr, "Error: Unexpected ObjRef instead of RA on stack");
        exit(EXIT_FAILURE);
    }
    pc = n.u.number;
}

void instDROP(int x) {
    for(int i=0; i<x; i++) {
        pop();
    }
}

void instPUSHR(void) {
    pushObj(rvr);
}

void instPOPR(void) {
    StackSlot n = pop();
    if(!n.isObjRef) {
        fprintf(stderr, "Error: Stackslot doesnt hold ObjRef\n");
        exit(EXIT_FAILURE);
    }
    rvr = n.u.objRef;
}

void instDUP(void) {
    //alternative: push(stack[sp-1])
    StackSlot n = pop();
    if(n.isObjRef) {
        pushObj(n.u.objRef);
        pushObj(n.u.objRef);
    } else {
        pushInt(n.u.number);
        pushInt(n.u.number);
    }
}

void instNEW(int x) {
    pushObj(newCompoundObject(x));
}

void instGETF(int x) {
    StackSlot n = pop();
    if(!n.isObjRef) {
        fprintf(stderr, "Error: Stackslot doesnt hold ObjRef\n");
        exit(EXIT_FAILURE);
    }
    if(n.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(IS_PRIMITIVE(n.u.objRef)) {
        fprintf(stderr, "Error: Object is not a compound object\n");
        exit(EXIT_FAILURE);
    }
    if(x>=GET_ELEMENT_COUNT(n.u.objRef)||x<0) {
        fprintf(stderr, "Error: Index is higher than number of objects\n");
        exit(EXIT_FAILURE);
    }
    pushObj(((ObjRef *)n.u.objRef->data)[x]);
}

void instPUTF(int x) {
    StackSlot value = pop();
    StackSlot object = pop();
    if(!(value.isObjRef&&object.isObjRef)) {
        fprintf(stderr, "Error: Stackslot doesnt hold ObjRef\n");
        exit(EXIT_FAILURE);
    }
    if(object.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(IS_PRIMITIVE(object.u.objRef)) {
        fprintf(stderr, "Error: Object is not a compound object\n");
        exit(EXIT_FAILURE);
    }
    if(x>=GET_ELEMENT_COUNT(object.u.objRef)||x<0) {
        fprintf(stderr, "Error: Index is higher than number of objects\n");
        exit(EXIT_FAILURE);
    }
    ((ObjRef *)object.u.objRef->data)[x] = value.u.objRef;
}

void instNEWA(void) {
    StackSlot n = pop();
    if(!n.isObjRef) {
        fprintf(stderr, "Error: Stackslot doesnt hold ObjRef\n");
        exit(EXIT_FAILURE);
    }
    if(n.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!IS_PRIMITIVE(n.u.objRef)) {
        fprintf(stderr, "Error: Object is not a primitive object\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = n.u.objRef;
    pushObj(newCompoundObject(bigToInt()));
}

void instGETFA(void) {
    StackSlot index = pop();
    StackSlot array = pop();
    if(!(index.isObjRef&&array.isObjRef)) {
        fprintf(stderr, "Error: Stackslot doesnt hold ObjRef\n");
        exit(EXIT_FAILURE);
    }
    if(index.u.objRef==NULL||array.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(!IS_PRIMITIVE(index.u.objRef)) {
        fprintf(stderr, "Error: Index is not a primitive object\n");
        exit(EXIT_FAILURE);
    }
    if(IS_PRIMITIVE(array.u.objRef)) {
        fprintf(stderr, "Error: Array is not a compound object\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = index.u.objRef;
    int x = bigToInt();
    if(x>=GET_ELEMENT_COUNT(array.u.objRef)||x<0) {
        fprintf(stderr, "Error: Index out of bounds\n");
        exit(EXIT_FAILURE);
    }
    pushObj(((ObjRef *)array.u.objRef->data)[x]);
}

void instPUTFA(void) {
    StackSlot value = pop();
    StackSlot index = pop();
    StackSlot array = pop();
    if(!(value.isObjRef&&index.isObjRef&&array.isObjRef)) {
        fprintf(stderr, "Error: Stackslot doesnt hold ObjRef\n");
        exit(EXIT_FAILURE);
    }
    if(index.u.objRef==NULL||array.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(IS_PRIMITIVE(array.u.objRef)) {
        fprintf(stderr, "Error: Array is not a compound object\n");
        exit(EXIT_FAILURE);
    }
    if(!IS_PRIMITIVE(index.u.objRef)) {
        fprintf(stderr, "Error: Index is not a primitive object\n");
        exit(EXIT_FAILURE);
    }
    bip.op1 = index.u.objRef;
    int x = bigToInt();
    if(x>=GET_ELEMENT_COUNT(array.u.objRef)||x<0) {
        fprintf(stderr, "Error: Index is higher than number of objects\n");
        exit(EXIT_FAILURE);
    }
    ((ObjRef *)array.u.objRef->data)[x] = value.u.objRef;
}

void instGETSZ(void) {
    StackSlot n = pop();
    if(!n.isObjRef) {
        fprintf(stderr, "Error: Stackslot doesnt hold ObjRef\n");
        exit(EXIT_FAILURE);
    }
    if(n.u.objRef==NULL) {
        fprintf(stderr, "Error: Trying to access NIL-pointer\n");
        exit(EXIT_FAILURE);
    }
    if(IS_PRIMITIVE(n.u.objRef)) bigFromInt(-1);
    else bigFromInt(GET_ELEMENT_COUNT(n.u.objRef));
    pushObj(bip.res);
}

void instPUSHN(void) {
    pushObj(NULL);
}

void instREFEQ(void) {
    StackSlot n1 = pop();
    StackSlot n2 = pop();
    if(!(n1.isObjRef&&n2.isObjRef)) {
        fprintf(stderr, "Error: Stackslot doesnt hold ObjRef\n");
        exit(EXIT_FAILURE);
    }
    if(n1.u.objRef==n2.u.objRef) bigFromInt(1);
    else bigFromInt(0);
    pushObj(bip.res);
}

void instREFNE(void) {
    StackSlot n1 = pop();
    StackSlot n2 = pop();
    if(!(n1.isObjRef&&n2.isObjRef)) {
        fprintf(stderr, "Error: Stackslot doesnt hold ObjRef\n");
        exit(EXIT_FAILURE);
    }
    if(n1.u.objRef==n2.u.objRef) bigFromInt(0);
    else bigFromInt(1);
    pushObj(bip.res);
}