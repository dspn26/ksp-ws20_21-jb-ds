//
// Created by Daniel Spengler on 16.12.2020.
//

#ifndef KSP_WS20_21_JB_DS_TYPES_H
#define KSP_WS20_21_JB_DS_TYPES_H
#include <stdbool.h>
#include <support.h>
typedef struct {
    bool isObjRef;
    union {
        ObjRef objRef;
        int number;
    } u;
} StackSlot;

#endif //KSP_WS20_21_JB_DS_TYPES_H
