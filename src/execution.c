//
// Created by Janek on 18.11.2020.
//

#include <stdio.h>
#include <stdlib.h>
#include "defines.h"
#include "execution.h"
#include "instructions.h"
#include "stack.h"
#include "debug.h"
#include "input.h"

int pc = 0;
int debug = 0;

void executeProgram(unsigned int *program) {
    pc = sp = fp = 0;
    unsigned int opcode = 256;
    while(pc<instCount&&opcode!= HALT) {
        unsigned int code = program[pc];
        opcode = decodeOpcode(code);
        int immediatevalue = decodeImmediate(code);
        //when the breakpoint is the next command, activate debug and remove the breakpoint
        if(breakpoint == pc) {
            debug = 1;
            breakpoint = -1;
        }
        if(debug) {
            debugProgram(immediatevalue, opcode, pc);
        }
        pc++;
        callInstruction(immediatevalue, opcode);
    }
}


void printProgram(unsigned int *program) {
    int printpc=0;
    unsigned int opcode = 256;
    while(printpc < instCount) {
        unsigned int code = program[printpc];
        opcode = decodeOpcode(code);
        int immediatevalue = decodeImmediate(code);
        printInstruction(immediatevalue, opcode, printpc);
        printpc++;
    }
}

unsigned int decodeOpcode(unsigned int code) {
    return code>>24;
}

int decodeImmediate(unsigned int code) {
    return SIGN_EXTEND(IMMEDIATE(code));
}

void callInstruction(int immediatevalue, unsigned int opcode) {
    switch (opcode) {
        case HALT:
            instHALT();
            break;
        case PUSHC:
            instPUSHC(immediatevalue);
            break;
        case ADD:
            instADD();
            break;
        case SUB:
            instSUB();
            break;
        case MUL:
            instMUL();
            break;
        case DIV:
            instDIV();
            break;
        case MOD:
            instMOD();
            break;
        case RDINT:
            instRDINT();
            break;
        case WRINT:
            instWRINT();
            break;
        case RDCHR:
            instRDCHR();
            break;
        case WRCHR:
            instWRCHR();
            break;
        case PUSHG:
            instPUSHG(immediatevalue);
            break;
        case POPG:
            instPOPG(immediatevalue);
            break;
        case ASF:
            instASF(immediatevalue);
            break;
        case RSF:
            instRSF();
            break;
        case PUSHL:
            instPUSHL(immediatevalue);
            break;
        case POPL:
            instPOPL(immediatevalue);
            break;
        case EQ:
            instEQ();
            break;
        case NE:
            instNE();
            break;
        case LT:
            instLT();
            break;
        case LE:
            instLE();
            break;
        case GT:
            instGT();
            break;
        case GE:
            instGE();
            break;
        case JMP:
            instJMP(immediatevalue);
            break;
        case BRF:
            instBRF(immediatevalue);
            break;
        case BRT:
            instBRT(immediatevalue);
            break;
        case CALL:
            instCALL(immediatevalue);
            break;
        case RET:
            instRET();
            break;
        case DROP:
            instDROP(immediatevalue);
            break;
        case PUSHR:
            instPUSHR();
            break;
        case POPR:
            instPOPR();
            break;
        case DUP:
            instDUP();
            break;
        case NEW:
            instNEW(immediatevalue);
            break;
        case GETF:
            instGETF(immediatevalue);
            break;
        case PUTF:
            instPUTF(immediatevalue);
            break;
        case NEWA:
            instNEWA();
            break;
        case GETFA:
            instGETFA();
            break;
        case PUTFA:
            instPUTFA();
            break;
        case GETSZ:
            instGETSZ();
            break;
        case PUSHN:
            instPUSHN();
            break;
        case REFEQ:
            instREFEQ();
            break;
        case REFNE:
            instREFNE();
            break;
        default:
            fprintf(stderr, "Error: Unknown opcode %d", opcode);
            exit(EXIT_FAILURE);
    }
}

void printInstruction(int immediatevalue, unsigned int opcode, int printpc) {
    printf("%03d: ", printpc);
    switch (opcode) {
        case HALT:
            printf("HALT\n");
            break;
        case PUSHC:
            printf("PUSHC %d\n", immediatevalue);
            break;
        case ADD:
            printf("ADD\n");
            break;
        case SUB:
            printf("SUB\n");
            break;
        case MUL:
            printf("MUL\n");
            break;
        case DIV:
            printf("DIV\n");
            break;
        case MOD:
            printf("MOD\n");
            break;
        case RDINT:
            printf("RDINT\n");
            break;
        case WRINT:
            printf("WRINT\n");
            break;
        case RDCHR:
            printf("RDCHR\n");
            break;
        case WRCHR:
            printf("WRCHR\n");
            break;
        case PUSHG:
            printf("PUSHG %d\n", immediatevalue);
            break;
        case POPG:
            printf("POPG %d\n", immediatevalue);
            break;
        case ASF:
            printf("ASF %d\n", immediatevalue);
            break;
        case RSF:
            printf("RSF\n");
            break;
        case PUSHL:
            printf("PUSHL %d\n", immediatevalue);
            break;
        case POPL:
            printf("POPL %d\n", immediatevalue);
            break;
        case EQ:
            printf("EQ\n");
            break;
        case NE:
            printf("NE\n");
            break;
        case LT:
            printf("LT\n");
            break;
        case LE:
            printf("LE\n");
            break;
        case GT:
            printf("GT\n");
            break;
        case GE:
            printf("GE\n");
            break;
        case JMP:
            printf("JMP %d\n", immediatevalue);
            break;
        case BRF:
            printf("BRF %d\n", immediatevalue);
            break;
        case BRT:
            printf("BRT %d\n", immediatevalue);
            break;
        case CALL:
            printf("CALL %d\n", immediatevalue);
            break;
        case RET:
            printf("RET\n");
            break;
        case DROP:
            printf("DROP %d\n", immediatevalue);
            break;
        case PUSHR:
            printf("PUSHR\n");
            break;
        case POPR:
            printf("POPR\n");
            break;
        case DUP:
            printf("DUP\n");
            break;
        case NEW:
            printf("NEW %d\n", immediatevalue);
            break;
        case GETF:
            printf("GETF %d\n", immediatevalue);
            break;
        case PUTF:
            printf("PUTF %d\n", immediatevalue);
            break;
        case NEWA:
            printf("NEWA\n");
            break;
        case GETFA:
            printf("GETFA\n");
            break;
        case PUTFA:
            printf("PUTFA\n");
            break;
        case GETSZ:
            printf("GETSZ\n");
            break;
        case PUSHN:
            printf("PUSHN\n");
            break;
        case REFEQ:
            printf("REFEQ\n");
            break;
        case REFNE:
            printf("REFNE\n");
            break;
        default:
            fprintf(stderr, "Error: Unknown opcode %d", opcode);
            exit(EXIT_FAILURE);
    }
}

