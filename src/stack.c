//
// Created by Daniel Spengler on 14.11.2020.
//

#include <stdio.h>
#include "defines.h"
#include "stack.h"
#include <stdlib.h>
#include "debug.h"
#include "input.h"
#include <bigint.h>

int sp = 0;
int fp = 0;

// Array representing the stack
StackSlot* stack;

//push informational data
void pushInt(int x) {
    // printf("Pushing %d on stack at index %d", x, sp);
    if(sp==(stackSize*1024)/sizeof(StackSlot)) {
        fprintf(stderr, "ERROR: stack overflow - maximum stack size exceeded");
        exit(EXIT_FAILURE);
    }
    StackSlot s;
    s.isObjRef = false;
    s.u.number = x;
    stack[sp++] = s;
}

void pushObj(ObjRef objRef) {
    // printf("Pushing %d on stack at index %d", x, sp);
    if(sp==(stackSize*1024)/sizeof(StackSlot)) {
        fprintf(stderr, "ERROR: stack overflow - maximum stack size exceeded\n");
        exit(EXIT_FAILURE);
    }
    StackSlot s;
    s.isObjRef = true;
    s.u.objRef = objRef;
    stack[sp++] = s;
    //bip.op1 = stack[sp-1].u.objRef;
    //printf("pushed: %d\n", bigToInt());
}

StackSlot pop() {
    // Pop values are not being deleted, they are set to 0
    // printf("Popped value %d at index %d", stack[sp-1], sp);
    if(sp==0) {
        fprintf(stderr, "ERROR: stack underflow - no elements in stack");
        exit(EXIT_FAILURE);
    }
    StackSlot output = stack[--sp];
    return output;
}

void printStack() {
    printf("Stack:\n");
    if(sp == fp) printf("%04d: SP, FP ->\n", sp);
    else printf("%04d: SP ->\n", sp);
    for(int i = sp-1; i >= 0; i--) {
        if(stack[i].isObjRef) {
            if(sp == fp) {
                printf("%04d:           ", i);
                inspectObjRef(stack[i].u.objRef, false);
            }
            else if (i == fp) {
                printf("%04d: FP -> ", i);
                inspectObjRef(stack[i].u.objRef, false);
            }
            else {
                printf("%04d:       ", i);
                inspectObjRef(stack[i].u.objRef, false);
            }
        } else {
            if(sp == fp) printf("%04d:        -> meta data, value: %d\n", i, stack[i].u.number);
            else if (i == fp) printf("%04d: FP -> meta data, value: %d\n", i, stack[i].u.number);
            else printf("%04d:       meta data, value: %d\n", i, stack[i].u.number);
        }
    }
}