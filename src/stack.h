//
// Created by Daniel Spengler on 14.11.2020.
//
#ifndef STACK_H
#define STACK_H
#include "types.h"

extern int sp;
extern int fp;
extern StackSlot* stack;
void pushInt(int x);
void pushObj(ObjRef objRef);
StackSlot pop(void);
void printStack(void);

#endif //STACK_H