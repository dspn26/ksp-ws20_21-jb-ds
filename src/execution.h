//
// Created by Janek on 18.11.2020.
//

#ifndef KSP_WS20_21_JB_DS_EXECUTION_H
#define KSP_WS20_21_JB_DS_EXECUTION_H

extern int pc;
extern int debug;
void executeProgram(unsigned int *program);
void printProgram(unsigned int *program);
unsigned int decodeOpcode(unsigned int code);
int decodeImmediate(unsigned int code);
void callInstruction(int immediatevalue, unsigned int opdcode);
void printInstruction(int immediatevalue, unsigned int opdcode, int printpc);
#endif //KSP_WS20_21_JB_DS_EXECUTION_H
