//
// Created by Daniel Spengler on 06.12.2020.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "debug.h"
#include "execution.h"
#include "defines.h"
#include "stack.h"
#include "input.h"
#include "instructions.h"
#include <bigint.h>
#include <stdbool.h>

int breakpoint = -1;

void debugProgram(int immediatevalue, int opcode, int printpc) {
    int debugloop = 1;
    while(debugloop) {
        printf("\n");
        printInstruction(immediatevalue, opcode, printpc);
        printf("Enter debug command (\"help\" to list all commands): \n");
        char* input = getInput();
        if(strcmp(input, "help") == 0) printf("%s", DEBUGHELP);
        else if(strcmp(input, "list") == 0) printProgram(instPtr);
        else if(strcmp(input, "quit") == 0) {
            printf("Quitting NJVM\n");
            exit(EXIT_SUCCESS);
        }
        else if(strcmp(input, "step") == 0) debugloop = 0;
        else if(strcmp(input, "ps") == 0) printStack();
        else if(strcmp(input, "pd") == 0) printSDA();
        else if(strcmp(input, "prvr") == 0) printRVR();
        else if(strcmp(input, "insp") == 0) inspectStackSlot();
        else if(strcmp(input, "run") == 0) {
            debug = 0;
            debugloop = 0;
        }
        else if(strcmp(input, "bs") == 0) setBreakpoint();
        else if(strcmp(input, "br") == 0) removeBreakpoint();
        else if(strcmp(input, "bl") == 0) printBreakpoint();
    }
}

char* getInput(void) {
    char inputBuffer[30];
    fgets(inputBuffer, sizeof(inputBuffer), stdin);
    static char command[30];
    sscanf(inputBuffer, "%s", command);
    return command;
}

void printSDA(void) {
    if(sdaSize == 0) printf("No static data in this program.\n");
    else printf("Static Data Area:\n");
    for(int i = 0; i < sdaSize; i++) {
        if(*(sdaPtr+i)==NULL) printf("%04d: NULL\n", i);
        else {
            printf("%04d: ", i);
            inspectObjRef(*(sdaPtr + i), true);
        }
    }
}

void printRVR(void) {
    printf("RVR: (first level of sub-objects if existing)\n");
    inspectObjRef(rvr, true);
}

//TODO: also print payload content of objects
void inspectStackSlot(void) {
    int inputloop = 1;
    while(inputloop) {
        printf("Please enter StackSlot number or type abort for no quitting.\n");
        char inputBuffer[30];
        fgets(inputBuffer, sizeof(inputBuffer), stdin);
        char * rest[30];
        int input;
        if((input = strtol(inputBuffer, rest, 10))>0) {
            if(input>=sp) {
                printf("Requested number is higher than elements on stack.\n");
            } else {
                inputloop=0;
                if(stack[input].isObjRef) {
                    printf("Type 'complex' if you also want to print information object contained references:\n");
                    char* command = getInput();
                    bool b = false;
                    if(strcmp(command, "complex") == 0) b = true;
                    inspectObjRef(stack[input].u.objRef, b);
                } else {
                    printf("%04d: meta data, value: %d\n", input, stack[input].u.number);
                }
            }
        } else {
            sscanf(inputBuffer, "%s", *rest);
            if (strcmp(*rest, "abort")==0) {
                inputloop = 0;
            } else {
                printf("Invalid input\n");
            }
        }
    }
}

void inspectObjRef(ObjRef obj, bool complex) {
    if(obj==NULL) {
        printf("NIL\n");
    }
    else if(IS_PRIMITIVE(obj)) {
        bip.op1 = obj;
        printf("Primitive object: value ");
        bigPrint(stdout);
        printf("\n");
    }
    else {
        int elementCount = GET_ELEMENT_COUNT(obj);
        printf("Compound object: holds %d references\n", elementCount);
        if(complex) {
            for(int i=0; i<elementCount; i++) {
                printf("    ");
                inspectObjRef(((ObjRef *)obj->data)[i], false);
            }
        }
    }
}

void setBreakpoint(void) {
    int inputloop = 1;
    while(inputloop) {
        printf("Please enter breakpoint or type abort for no change (Setting a new breakpoint will remove the current breakpoint).\n");
        char inputBuffer[30];
        fgets(inputBuffer, sizeof(inputBuffer), stdin);
        char * rest[30];
        int input;
        if((input = strtol(inputBuffer, rest, 10))>0) {
            inputloop = 0;
            breakpoint = input;
        } else {
            sscanf(inputBuffer, "%s", *rest);
            if (strcmp(*rest, "abort")==0) {
                inputloop = 0;
            } else {
                printf("Breakpoint has to be a number greater than 0!\n");
            }
        }
    }
}

void removeBreakpoint(void) {
    if(breakpoint == -1) printf("No breakpoint to remove.\n");
    else {
        breakpoint = -1;
        printf("Removed breakpoint.\n");
    }
}

void printBreakpoint(void) {
    if(breakpoint == -1) printf("No breakpoint is set currently.\n");
    else printf("Breakpoint is currently set at: %d\n", breakpoint);
}