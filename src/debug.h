//
// Created by Daniel Spengler on 06.12.2020.
//

#ifndef KSP_WS20_21_JB_DS_DEBUG_H
#define KSP_WS20_21_JB_DS_DEBUG_H
#include <stdbool.h>
#include "types.h"
extern int breakpoint;
void debugProgram(int immediatevalue, int opcode, int printpc);
char* getInput(void);
void printSDA(void);
void printRVR(void);
void inspectStackSlot(void);
void setBreakpoint(void);
void removeBreakpoint(void);
void printBreakpoint(void);
void inspectObjRef(ObjRef obj, bool complex);
#endif //KSP_WS20_21_JB_DS_DEBUG_H
