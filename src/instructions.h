//
// Created by Janek on 14.11.2020.
//

#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H
#include "types.h"

extern ObjRef rvr;
void instHALT(void);
void instPUSHC(int x);
void instADD(void);
void instSUB(void);
void instMUL(void);
void instDIV(void);
void instMOD(void);
void instRDINT(void);
void instWRINT(void);
void instRDCHR(void);
void instWRCHR(void);
void instPUSHG(int x);
void instPOPG(int x);
void instASF(int x);
void instRSF(void);
void instPUSHL(int x);
void instPOPL(int x);
void instEQ(void);
void instNE(void);
void instLT(void);
void instLE(void);
void instGT(void);
void instGE(void);
void instJMP(int target);
void instBRF(int target);
void instBRT(int target);
void instCALL(int target);
void instRET(void);
void instDROP(int x);
void instPUSHR(void);
void instPOPR(void);
void instDUP(void);
void instNEW(int x);
void instGETF(int x);
void instPUTF(int x);
void instNEWA(void);
void instGETFA(void);
void instPUTFA(void);
void instGETSZ(void);
void instPUSHN(void);
void instREFEQ(void);
void instREFNE(void);
#endif //INSTRUCTIONS_H
