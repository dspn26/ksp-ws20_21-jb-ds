//
// Created by Daniel Spengler on 22.11.2020.
//

#ifndef INPUT_H
#define INPUT_H
#include "types.h"
extern unsigned int *instPtr;
extern ObjRef *sdaPtr;
extern char* heapPtr;
extern char* targetPtr;
extern char* targetEndPtr;
extern char* freePtr;
extern unsigned int instCount;
extern unsigned int sdaSize;
extern unsigned int stackSize;
extern unsigned int heapSize;
void readInput(char *file);
#endif //INPUT_H
