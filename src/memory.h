//
// Created by Janek on 20.01.2021.
//

#ifndef KSP_WS20_21_JB_DS_MEMORY_H
#define KSP_WS20_21_JB_DS_MEMORY_H
#include "support.h"

extern int purge ;
extern int gcstats;
extern int objCount;

ObjRef alloc(int dataSize);
void garbageCollector(void);
void swap(void);
ObjRef copyObjectToFreeMem(ObjRef orig);
void scan(void);
void root(void);
void purgePassiveHalfMemory(void);
void countObj(void);

#endif //KSP_WS20_21_JB_DS_MEMORY_H
