//
// Created by Daniel Spengler on 23.12.2020.
//

#include <support.h>
#include <stdio.h>
#include <stdlib.h>
#include "memory.h"

void fatalError(char *msg) {
    fprintf(stderr, "%s", msg);
    exit(EXIT_FAILURE);
}

ObjRef newPrimObject(int dataSize) {
    int objSize = sizeof(int) + sizeof(char) /*+ sizeof(ObjRef*)*/ + dataSize;
    if(dataSize<8) objSize = sizeof(int) + sizeof(char) + sizeof(void*);
    //round up to multiples of 4
    //if(objSize%4>0) objSize = ((objSize/4)*4)+4;
    ObjRef objRef;
    objRef = alloc(objSize);
    objRef->size = objSize;
    objRef->brokenHeart = 0;
    return objRef;
}