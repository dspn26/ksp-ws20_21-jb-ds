#include <stdio.h>
#include <string.h>
#include "execution.h"
#include "input.h"
#include "defines.h"
#include <stdlib.h>
#include "memory.h"
#include "stack.h"

int main(int argc, char *argv[]) {
    //checks if there was a command line arguments and responds to it

    if(argc >= 2) {
        for(int i = 1; i < argc-1; i++) {
            if(strcmp(argv[i], "--version") == 0) {
                printf("Ninja Virtual Machine version %d (compiled %s at %s)\n", VERSION, __DATE__, __TIME__);
                return 0;
            }
            else if(strcmp(argv[i], "--help") == 0) {
                printf("Usage: ./njvm [option]\n");
                printf("  --help          Show this help and exits\n");
                printf("  --version       Show NJVM version and exits\n");
                printf("  [file]          Loads the file and runs it (filename cannot start with \"--\"\n");
                printf("  [file] --debug  Loads the file and runs it in debug mode\n");
                return 0;
            }
            else if(strcmp(argv[i], "--debug") == 0) debug = 1;
            else if(strcmp(argv[i], "--stack") == 0) {
                if(argc >= i+1) {
                    stackSize = atoi(argv[++i]);
                }else {
                    fprintf(stderr, "Expected number after argument '--stack'");
                    exit(EXIT_FAILURE);
                }
            }
            else if(strcmp(argv[i], "--heap") == 0) {
                if(argc >= i+1) {
                    heapSize = atoi(argv[++i]);
                }else {
                    fprintf(stderr, "Expected number after argument '--heap'");
                    exit(EXIT_FAILURE);
                }
            }
            else if(strcmp(argv[i], "--gcstats") == 0) gcstats = 1;
            else if(strcmp(argv[i], "--gcpurge") == 0) purge = 1;
            else {
                printf("unknown argument '%s', try './njvm --help'", argv[1]);
                return 1;
            }
        }
        printf("Ninja Virtual Machine started\n");
        char *path = argv[argc-1];
        if(debug) {
            printf("----------------------------\n");
            printf("Reading file:\n\n");
        }
        readInput(path);
        if(debug) {
            printf("----------------------------\n");
            printf("Executing program:\n\n");
        }
        executeProgram(instPtr);
        if(debug) printf("----------------------------\n");

        printf("Ninja Virtual Machine stopped\n");
        free(instPtr);
        free(sdaPtr);
        free(heapPtr);
        free(stack);
        return 0;
    } else {
        //default behavior when no arguments where given
        printf("no arguments given, try './njvm --help'\n");
        return 1;
    }
}