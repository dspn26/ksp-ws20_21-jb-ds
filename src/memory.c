//
// Created by Janek on 20.01.2021.
//

#include "memory.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "support.h"
#include "input.h"
#include "stack.h"
#include "instructions.h"
#include <bigint.h>
#include "defines.h"

int purge = 0;
int gcstats = 0;
int objCount = 0;


ObjRef alloc(int dataSize) {
    //printf("free: %p, size: %d, target: %p, end: %p\n", freePtr, dataSize, freePtr+dataSize, targetEndPtr);
    if(freePtr+dataSize>=targetEndPtr) {
        garbageCollector();
        if(freePtr+dataSize>=targetEndPtr) {
            fprintf(stderr, "Error: Not enough heap space to allocate new object\n");
            exit(EXIT_FAILURE);
        }
    }
    ObjRef obj;
    obj = (ObjRef) freePtr;
    freePtr += dataSize;
    return obj;
}


void garbageCollector(void) {
    if(gcstats!=0) {
        printf("Garbage Collector triggered!\n");
        int oldCount = objCount;
        countObj();
        printf("New objects since last run: %d\n", objCount-oldCount);
    }
    swap();
    root();
    scan();
    if(purge != 0) purgePassiveHalfMemory();
    if(gcstats!=0) {
        countObj();
        printf("Number of vivid objects: %d\n", objCount);
        printf("Memory used by vivid objects: %ld\n", (long)freePtr-(long)targetPtr);
        printf("Free memory: %ld\n\n", (long)targetEndPtr-(long)freePtr);
    }
}

//switches the used half of the heap
void swap(void) {
    if(targetPtr==heapPtr) {
        targetPtr = targetEndPtr;
        //targetPtr = targetPtr + heapSize*1024/2;
        targetEndPtr = heapPtr + (heapSize*1024);
    } else {
        targetPtr = heapPtr;
        targetEndPtr = heapPtr + (heapSize*1024/2);
    }
    freePtr = targetPtr;
}


ObjRef relocate(ObjRef orig) {
    ObjRef copy;
    if(orig == NULL) {
        copy = NULL;
    } else {
        if(orig->brokenHeart!=0) {
            copy=((ObjRef*)orig->data)[0];
        } else {
            copy = copyObjectToFreeMem(orig);
            orig->brokenHeart = 1;
            ((ObjRef*)orig->data)[0] = copy;
        }
    }
    return copy;
}

void copyMemory(char* dest, ObjRef src, int count) {
    //printf("dest %p\nsrc %p\ncount: %d\nisPrimitive: %d\nbrokenHeart: %d\n", dest, src, count, IS_PRIMITIVE(src), src->brokenHeart);
    for(int i = 0; i<count; i++) {
        dest[i] = ((char*)src)[i];
    }
}

ObjRef copyObjectToFreeMem(ObjRef orig) {
    int origSize;
    if(IS_PRIMITIVE(orig)) {
        origSize = orig->size;
    } else {
        origSize = sizeof(int) + sizeof(char) + GET_ELEMENT_COUNT(orig)*(sizeof(ObjRef*));
    }
    //memcpy(freePtr, orig, origSize);
    copyMemory(freePtr, orig, origSize);
    ObjRef returnPtr = (ObjRef) freePtr;
    freePtr += origSize;
    return returnPtr;
}



void root(void) {
    for(int i = 0; i<sdaSize; i++) {
        sdaPtr[i] = relocate(sdaPtr[i]);
    }
    for(int i = 0; i<sp; i++) {
        if(stack[i].isObjRef) {
            stack[i].u.objRef = relocate(stack[i].u.objRef);
        }
    }
    rvr = relocate(rvr);
    bip.op1 = relocate(bip.op1);
    bip.op2 = relocate(bip.op2);
    bip.res = relocate(bip.res);
    bip.rem = relocate(bip.rem);
}

void scan(void) {
    char* scan = targetPtr;
    while(scan < freePtr) {
        ObjRef compObject = (ObjRef) scan;
        if(!IS_PRIMITIVE(compObject)) {
            for(int i = 0; i<GET_ELEMENT_COUNT(compObject); i++) {
                ((ObjRef*)compObject->data)[i] = relocate(((ObjRef*)compObject->data)[i]);
            }
            scan += sizeof(int) + sizeof(char) + GET_ELEMENT_COUNT(compObject)*(sizeof(ObjRef*));
        } else {
            scan += compObject->size;
        }
    }
}

void purgePassiveHalfMemory(void) {
    char* killPtr;
    if(targetPtr == heapPtr) killPtr = targetEndPtr;
    else killPtr = heapPtr;
    memset(killPtr, 0x0, heapSize * 1024 / 2);
}

void countObj(void) {
    objCount = 0;
 char* countPtr = targetPtr;
    while(countPtr<freePtr) {
        countPtr += IS_PRIMITIVE((ObjRef) countPtr) ? ((ObjRef) countPtr)->size : sizeof(int) + sizeof(char *) + GET_ELEMENT_COUNT((ObjRef) countPtr) * sizeof(void *);
        objCount++;
    }
}