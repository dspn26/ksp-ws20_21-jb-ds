//
// Created by Daniel Spengler on 22.11.2020.
//

#include <stdio.h>
#include <stdlib.h>
#include "input.h"
#include "execution.h"
#include "defines.h"
#include "stack.h"
#include <bigint.h>

unsigned int *instPtr = NULL;
ObjRef *sdaPtr = NULL;
char* heapPtr = NULL;
char* targetPtr = NULL;
char* targetEndPtr = NULL;
char* freePtr = NULL;
unsigned int instCount = 0;
unsigned int sdaSize = 0;
unsigned int stackSize = 64; // Default value
unsigned int heapSize = 8192;

void readInput(char *path) {
    //open the file
    FILE *fp = NULL;
    if((fp = fopen(path, "r")) == NULL) {
        fprintf(stderr, "Failed to open file '%s'", path);
        exit(EXIT_FAILURE);
    }

    unsigned int x = 0;
    //read the file format
    unsigned int format = 0;
    for(int i = 0; i<=3; i++) {
        if(fread(&x, 1, 1, fp)==0) {
            fprintf(stderr, "Error: failed to read input file");
            exit(EXIT_FAILURE);
        } else format |= x<<(24-8*i);
    }
    if(debug) printf("Format: %08x\n", format);
    if(format != 0x4e4a4246) {
        fprintf(stderr, "No NJBF!\n");
        exit(EXIT_FAILURE);
    }

    //check version
    if(fread(&x, 1, 4, fp)==0) {
        fprintf(stderr, "Error: failed to read input file");
        exit(EXIT_FAILURE);
    }
    if(debug) printf("Version: %d\n", x);
    if(x != VERSION) {
       fprintf(stderr, "Version not compatible mit this NVJM!");
       exit(EXIT_FAILURE);
    }

    // Allocate memory for instructions
    if(fread(&x, 1, 4, fp)==0) {
        fprintf(stderr, "Error: failed to read input file");
        exit(EXIT_FAILURE);
    }
    if(debug) printf("Instructions: %d\n", x);
    instCount = x;
    instPtr = malloc(instCount * sizeof(unsigned int));
    if(instCount!=0&&instPtr == NULL) {
        fprintf(stderr, "Error: memory allocation for instructions failed");
        exit(EXIT_FAILURE);
    }

    // Allocate memory for static variables
    if(fread(&x, 1, 4, fp)==0) {
        fprintf(stderr, "Error: failed to read input file");
        exit(EXIT_FAILURE);
    }
    if(debug) printf("Static variables: %d\n", x);
    sdaSize = x;
    sdaPtr = malloc(sdaSize * sizeof(ObjRef));
    if(sdaSize!=0&&sdaPtr == NULL) {
        fprintf(stderr, "Error: memory allocation for static data area failed");
        exit(EXIT_FAILURE);
    }
    for(int i = 0; i<sdaSize; i++) sdaPtr[i]=NULL;

    //read all instructions and save in program memory
    if((fread(instPtr, 4, instCount, fp))!=instCount) {
       fprintf(stderr, "Error reading instructions, count does not match.");
       exit(EXIT_FAILURE);
    }

    // Closing file
    if(fclose(fp) != 0) {
       fprintf(stderr, "Failed to close file.");
       exit(EXIT_FAILURE);
    }

    //Debug: Check read instructions
    if(debug) {
        for(int i = 0; i<instCount; i++) {
            printf("Instruction %d: Hex %08x / Opcode %d / Immediate: %d\n", i, *(instPtr+i), decodeOpcode(*(instPtr+i)), decodeImmediate(*(instPtr+i)));
        }
    }

    // Allocate heap
    if((heapPtr = malloc(heapSize*1024)) == NULL) {
        fprintf(stderr, "ERROR: Failed allocating heap.\n");
        exit(EXIT_FAILURE);
    }

    // Allocate stack
    if((stack = malloc(stackSize*1024)) == NULL) {
        fprintf(stderr, "ERROR: Failed allocating stack.\n");
        exit(EXIT_FAILURE);
    }

    //set pointers
    targetPtr = heapPtr;
    targetEndPtr = targetPtr + heapSize*1024/2;
    freePtr = targetPtr;
    //printf("heapPtr: %p, targetPtr: %p, targetEndPtr: %p, freePtr: %p", heapPtr, targetPtr, targetEndPtr, freePtr);


    //set all BIP registers to NULL
    bip.op1=NULL;
    bip.op2=NULL;
    bip.res=NULL;
    bip.rem=NULL;
}