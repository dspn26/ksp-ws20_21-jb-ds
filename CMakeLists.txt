cmake_minimum_required(VERSION 3.16)
project(ksp_ws20_21_jb_ds C)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES src/njvm.c src/stack.c src/execution.c src/instructions.c src/input.c src/input.c src/njvm.c src/debug.c src/types.h src/support.c src/memory.c)

set(BIGINT_DIR src/bigint/build)

include_directories(${BIGINT_DIR}/include)

link_directories(${BIGINT_DIR}/lib)

add_executable(njvm ${SOURCE_FILES})

target_link_libraries(njvm bigint)