# NJVM debugger specification
###### by Daniel Spengler and Janek Berg (KSP Praktikum WS2020)

## 1. Starting the debugger
The interactive debugger for NJVM can be started whenever a file is loaded and run by putting the argument "--debug" behind the filepath.
* For example: '**./njvm test.bin --debug**' will load the file 'test.bin' in debug mode.

## 2. Default debugging output
When in debug mode, the NJVM prints additional information while loading and running the file, containing the following:
##### reading the file:
* file format String (as hexadecimal output)
* version number of the file
* number of instructions contained in the file
* number of variables to be stored in the static data area
* list of all instructions with their 32-bit hex code, opcode and immediate value
##### executing the file:
* before the debugger expects a command from the user, the next instruction is shown with number, name and immediate value (if such exists)

## 3. Commands for interactive use
When running a file in debug mode, the debugger will ask the user to type a command instead of running the file.

The following commands are available:
* **help** will show an overview of all commands.
* **quit** will quit the NJVM.
* **list** will print all instructions contained in current the program.
* **step** will run the next instruction.
* **run** will run the program until a breakpoint or until the execution is finished.
* **ps** will print the current stack with its values as well as the position of the stackpointer and framepointer.
* **pd** will print the current values stored in the static data area.
* **prvr** will print the value currently stored in the return value register.
* **bs** will set a new breakpoint. After using the command, the user is asked to type the instruction number of the new breakpoint.
  * When a breakpoint is set at instruction n, program execution with 'run' will stop the execution **before** instruction n is executed and go back to debugging mode. The user can then use new debug commands.
  * Since there can only be set one breakpoint at a time, please mind that setting a breakpoint will overwrite the old breakpoint.
  * Please mind that instructions are counted beginning with **zero**.
* **br** will remove the current breakpoint if one is set.
* **bl** will list the current breakpoint.