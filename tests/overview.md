# Handwritten .asm-files


## basic running tests
* `basictest01.asm` calculates (3+4)*(10-6)
* `basictest02.asm` calculates (-2)*x+3
* `basictest03.asm` reads a number and prints it twice

## edge cases
* `empty.asm` as completly empty file


## global and local variables
* `glvars01.asm` for global variables
* `glvars02.asm` for local variables
* `glvars03.asm` mixes local and global variables
* `glvars04.asm` computes iterative gcd with global variables
* `glvars05.asm` computes iterative gcd with local variables


## Function CALL/RET tests
* `funcTest01.asm` call/return without arguments
* `funcTest02.asm` call/return with arguments but without return value
* `funcTest03.asm` call/return with return value but without arguments
* `funcTest04.asm` call/return with arguments and return value


# Compiled files from .nj-code

## Function CALL/RET tests
* `funcTest01.asm` computes iterative gcd with local variables
* `funcTest02.asm` computes recursive factorial
* `funcTest03.asm` computes iterative factorial

## Custom Program tests
* `bigtest.asm` consists of all availables instructions for systematic tests
* `customtest01.asm` takes x, y and z, calculates x+y on z>=0, else x-y
* `customtest02.asm` calculates recursive fibonacci, also used for testing stackoverflow
* `customtest03.asm` takes x, y and z and check if y<z<x
* `customtest04.asm` takes x and y, prints all numbers i with x<i<y
* `customtest05.asm` takes 5 numbers, finds the lowest and calculates the sum without it