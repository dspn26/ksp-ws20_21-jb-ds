//
// version
//
	.vers	4

//
// execution framework
//
__start:
	call	_main
	call	_exit
__stop:
	jmp	__stop

//
// Integer readInteger()
//
_readInteger:
	asf	0
	rdint
	popr
	rsf
	ret

//
// void writeInteger(Integer)
//
_writeInteger:
	asf	0
	pushl	-3
	wrint
	rsf
	ret

//
// Character readCharacter()
//
_readCharacter:
	asf	0
	rdchr
	popr
	rsf
	ret

//
// void writeCharacter(Character)
//
_writeCharacter:
	asf	0
	pushl	-3
	wrchr
	rsf
	ret

//
// Integer char2int(Character)
//
_char2int:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// Character int2char(Integer)
//
_int2char:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// void exit()
//
_exit:
	asf	0
	halt
	rsf
	ret

//
// void main()
//
_main:
	asf	6
	call	_readInteger
	pushr
	popl	0
	call	_readInteger
	pushr
	popl	1
	call	_readInteger
	pushr
	popl	2
	call	_readInteger
	pushr
	popl	3
	call	_readInteger
	pushr
	popl	4
	pushl	0
	pushl	1
	pushl	2
	pushl	3
	pushl	4
	call	_getMin
	drop	5
	pushr
	popl	5
	pushl	0
	pushl	1
	pushl	2
	pushl	3
	pushl	4
	pushl	5
	call	_sumWithoutMin
	drop	6
	pushr
	call	_writeInteger
	drop	1
__0:
	rsf
	ret

//
// Integer getMin(Integer, Integer, Integer, Integer, Integer)
//
_getMin:
	asf	1
	pushl	-7
	popl	0
	pushl	-6
	pushl	0
	lt
	brf	__2
	pushl	-6
	popl	0
__2:
	pushl	-5
	pushl	0
	lt
	brf	__3
	pushl	-5
	popl	0
__3:
	pushl	-4
	pushl	0
	lt
	brf	__4
	pushl	-4
	popl	0
__4:
	pushl	-3
	pushl	0
	lt
	brf	__5
	pushl	-3
	popl	0
__5:
	pushl	0
	popr
	jmp	__1
__1:
	rsf
	ret

//
// Integer sumWithoutMin(Integer, Integer, Integer, Integer, Integer, Integer)
//
_sumWithoutMin:
	asf	3
	pushc	0
	popl	2
	pushc	0
	popl	0
	pushc	1
	popl	1
	pushl	-8
	pushl	-3
	eq
	dup
	brf	__9
	drop	1
	pushc	1
	pushl	0
	sub
__9:
	brf	__7
	pushc	1
	popl	0
	jmp	__8
__7:
	pushl	2
	pushl	-8
	add
	popl	2
__8:
	pushl	-7
	pushl	-3
	eq
	dup
	brf	__12
	drop	1
	pushc	1
	pushl	0
	sub
__12:
	brf	__10
	pushc	1
	popl	0
	jmp	__11
__10:
	pushl	2
	pushl	-7
	add
	popl	2
__11:
	pushl	-6
	pushl	-3
	eq
	dup
	brf	__15
	drop	1
	pushc	1
	pushl	0
	sub
__15:
	brf	__13
	pushc	1
	popl	0
	jmp	__14
__13:
	pushl	2
	pushl	-6
	add
	popl	2
__14:
	pushl	-5
	pushl	-3
	eq
	dup
	brf	__18
	drop	1
	pushc	1
	pushl	0
	sub
__18:
	brf	__16
	pushc	1
	popl	0
	jmp	__17
__16:
	pushl	2
	pushl	-5
	add
	popl	2
__17:
	pushl	-4
	pushl	-3
	eq
	dup
	brf	__21
	drop	1
	pushc	1
	pushl	0
	sub
__21:
	brf	__19
	pushc	1
	popl	0
	jmp	__20
__19:
	pushl	2
	pushl	-4
	add
	popl	2
__20:
	pushl	2
	popr
	jmp	__6
__6:
	rsf
	ret
