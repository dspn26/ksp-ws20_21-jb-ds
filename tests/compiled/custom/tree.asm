//
// version
//
	.vers	7

//
// execution framework
//
__start:
	call	_main
	call	_exit
__stop:
	jmp	__stop

//
// Integer readInteger()
//
_readInteger:
	asf	0
	rdint
	popr
	rsf
	ret

//
// void writeInteger(Integer)
//
_writeInteger:
	asf	0
	pushl	-3
	wrint
	rsf
	ret

//
// Character readCharacter()
//
_readCharacter:
	asf	0
	rdchr
	popr
	rsf
	ret

//
// void writeCharacter(Character)
//
_writeCharacter:
	asf	0
	pushl	-3
	wrchr
	rsf
	ret

//
// Integer char2int(Character)
//
_char2int:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// Character int2char(Integer)
//
_int2char:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// void exit()
//
_exit:
	asf	0
	halt
	rsf
	ret

//
// void writeString(String)
//
_writeString:
	asf	1
	pushc	0
	popl	0
	jmp	_writeString_L2
_writeString_L1:
	pushl	-3
	pushl	0
	getfa
	call	_writeCharacter
	drop	1
	pushl	0
	pushc	1
	add
	popl	0
_writeString_L2:
	pushl	0
	pushl	-3
	getsz
	lt
	brt	_writeString_L1
	rsf
	ret

//
// record { Integer isLeaf; Integer number; TreeNode leftChild; TreeNode rightChild; Character operator; } newLeafNode(Integer)
//
_newLeafNode:
	asf	1
	new	5
	popl	0
	pushl	0
	pushc	1
	putf	0
	pushl	0
	pushl	-3
	putf	1
	pushl	0
	popr
	jmp	__0
__0:
	rsf
	ret

//
// record { Integer isLeaf; Integer number; TreeNode leftChild; TreeNode rightChild; Character operator; } newInnerNode(Character, record { Integer isLeaf; Integer number; TreeNode leftChild; TreeNode rightChild; Character operator; }, record { Integer isLeaf; Integer number; TreeNode leftChild; TreeNode rightChild; Character operator; })
//
_newInnerNode:
	asf	1
	new	5
	popl	0
	pushl	0
	pushc	0
	putf	0
	pushl	0
	pushl	-5
	putf	4
	pushl	0
	pushl	-4
	putf	2
	pushl	0
	pushl	-3
	putf	3
	pushl	0
	popr
	jmp	__1
__1:
	rsf
	ret

//
// void main()
//
_main:
	asf	1
	pushc	45
	pushc	5
	call	_newLeafNode
	drop	1
	pushr
	pushc	42
	pushc	43
	pushc	1
	call	_newLeafNode
	drop	1
	pushr
	pushc	3
	call	_newLeafNode
	drop	1
	pushr
	call	_newInnerNode
	drop	3
	pushr
	pushc	45
	pushc	4
	call	_newLeafNode
	drop	1
	pushr
	pushc	7
	call	_newLeafNode
	drop	1
	pushr
	call	_newInnerNode
	drop	3
	pushr
	call	_newInnerNode
	drop	3
	pushr
	call	_newInnerNode
	drop	3
	pushr
	popl	0
	pushl	0
	pushc	0
	call	_printTree
	drop	2
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	0
	call	_evaluateTree
	drop	1
	pushr
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	0
	call	_generateAssembler
	drop	1
__2:
	rsf
	ret

//
// void printTree(record { Integer isLeaf; Integer number; TreeNode leftChild; TreeNode rightChild; Character operator; }, Integer)
//
_printTree:
	asf	1
	pushc	0
	popl	0
	jmp	__5
__4:
	pushc	32
	call	_writeCharacter
	drop	1
	pushc	32
	call	_writeCharacter
	drop	1
	pushl	0
	pushc	1
	add
	popl	0
__5:
	pushl	0
	pushl	-3
	lt
	brt	__4
__6:
	pushl	-4
	getf	0
	pushc	1
	eq
	brf	__7
	pushc	110
	call	_writeCharacter
	drop	1
	pushc	101
	call	_writeCharacter
	drop	1
	pushc	119
	call	_writeCharacter
	drop	1
	pushc	76
	call	_writeCharacter
	drop	1
	pushc	101
	call	_writeCharacter
	drop	1
	pushc	97
	call	_writeCharacter
	drop	1
	pushc	102
	call	_writeCharacter
	drop	1
	pushc	78
	call	_writeCharacter
	drop	1
	pushc	111
	call	_writeCharacter
	drop	1
	pushc	100
	call	_writeCharacter
	drop	1
	pushc	101
	call	_writeCharacter
	drop	1
	pushc	40
	call	_writeCharacter
	drop	1
	pushl	-4
	getf	1
	call	_writeInteger
	drop	1
	pushc	41
	call	_writeCharacter
	drop	1
	jmp	__8
__7:
	pushc	110
	call	_writeCharacter
	drop	1
	pushc	101
	call	_writeCharacter
	drop	1
	pushc	119
	call	_writeCharacter
	drop	1
	pushc	73
	call	_writeCharacter
	drop	1
	pushc	110
	call	_writeCharacter
	drop	1
	pushc	110
	call	_writeCharacter
	drop	1
	pushc	101
	call	_writeCharacter
	drop	1
	pushc	114
	call	_writeCharacter
	drop	1
	pushc	78
	call	_writeCharacter
	drop	1
	pushc	111
	call	_writeCharacter
	drop	1
	pushc	100
	call	_writeCharacter
	drop	1
	pushc	101
	call	_writeCharacter
	drop	1
	pushc	40
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	-3
	pushc	1
	add
	popl	-3
	pushc	0
	popl	0
	jmp	__10
__9:
	pushc	32
	call	_writeCharacter
	drop	1
	pushc	32
	call	_writeCharacter
	drop	1
	pushl	0
	pushc	1
	add
	popl	0
__10:
	pushl	0
	pushl	-3
	lt
	brt	__9
__11:
	pushc	39
	call	_writeCharacter
	drop	1
	pushl	-4
	getf	4
	call	_writeCharacter
	drop	1
	pushc	39
	call	_writeCharacter
	drop	1
	pushc	44
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	-4
	getf	2
	pushl	-3
	call	_printTree
	drop	2
	pushc	44
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	-4
	getf	3
	pushl	-3
	call	_printTree
	drop	2
	pushc	41
	call	_writeCharacter
	drop	1
__8:
__3:
	rsf
	ret

//
// Integer evaluateTree(record { Integer isLeaf; Integer number; TreeNode leftChild; TreeNode rightChild; Character operator; })
//
_evaluateTree:
	asf	2
	pushl	-3
	getf	0
	pushc	1
	eq
	brf	__13
	pushl	-3
	getf	1
	popr
	jmp	__12
	jmp	__14
__13:
	pushl	-3
	getf	2
	call	_evaluateTree
	drop	1
	pushr
	popl	0
	pushl	-3
	getf	3
	call	_evaluateTree
	drop	1
	pushr
	popl	1
	pushl	-3
	getf	4
	pushc	43
	eq
	brf	__15
	pushl	0
	pushl	1
	add
	popr
	jmp	__12
	jmp	__16
__15:
	pushl	-3
	getf	4
	pushc	45
	eq
	brf	__17
	pushl	0
	pushl	1
	sub
	popr
	jmp	__12
	jmp	__18
__17:
	pushl	-3
	getf	4
	pushc	42
	eq
	brf	__19
	pushl	0
	pushl	1
	mul
	popr
	jmp	__12
	jmp	__20
__19:
	pushl	-3
	getf	4
	pushc	47
	eq
	brf	__21
	pushl	0
	pushl	1
	div
	popr
	jmp	__12
	jmp	__22
__21:
	pushl	-3
	getf	4
	pushc	37
	eq
	brf	__23
	pushl	0
	pushl	1
	mod
	popr
	jmp	__12
__23:
__22:
__20:
__18:
__16:
__14:
__12:
	rsf
	ret

//
// void generateAssembler(record { Integer isLeaf; Integer number; TreeNode leftChild; TreeNode rightChild; Character operator; })
//
_generateAssembler:
	asf	0
	pushl	-3
	call	_generateAssemblerNode
	drop	1
	pushc	119
	call	_writeCharacter
	drop	1
	pushc	114
	call	_writeCharacter
	drop	1
	pushc	105
	call	_writeCharacter
	drop	1
	pushc	110
	call	_writeCharacter
	drop	1
	pushc	116
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushc	104
	call	_writeCharacter
	drop	1
	pushc	97
	call	_writeCharacter
	drop	1
	pushc	108
	call	_writeCharacter
	drop	1
	pushc	116
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
__24:
	rsf
	ret

//
// void generateAssemblerNode(record { Integer isLeaf; Integer number; TreeNode leftChild; TreeNode rightChild; Character operator; })
//
_generateAssemblerNode:
	asf	2
	pushl	-3
	getf	0
	pushc	1
	eq
	brf	__26
	pushc	112
	call	_writeCharacter
	drop	1
	pushc	117
	call	_writeCharacter
	drop	1
	pushc	115
	call	_writeCharacter
	drop	1
	pushc	104
	call	_writeCharacter
	drop	1
	pushc	99
	call	_writeCharacter
	drop	1
	pushc	32
	call	_writeCharacter
	drop	1
	pushl	-3
	getf	1
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	jmp	__27
__26:
	pushl	-3
	getf	2
	call	_generateAssemblerNode
	drop	1
	pushl	-3
	getf	3
	call	_generateAssemblerNode
	drop	1
	pushl	-3
	getf	4
	pushc	43
	eq
	brf	__28
	pushc	97
	call	_writeCharacter
	drop	1
	pushc	100
	call	_writeCharacter
	drop	1
	pushc	100
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	jmp	__29
__28:
	pushl	-3
	getf	4
	pushc	45
	eq
	brf	__30
	pushc	115
	call	_writeCharacter
	drop	1
	pushc	117
	call	_writeCharacter
	drop	1
	pushc	98
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	jmp	__31
__30:
	pushl	-3
	getf	4
	pushc	42
	eq
	brf	__32
	pushc	109
	call	_writeCharacter
	drop	1
	pushc	117
	call	_writeCharacter
	drop	1
	pushc	108
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	jmp	__33
__32:
	pushl	-3
	getf	4
	pushc	47
	eq
	brf	__34
	pushc	100
	call	_writeCharacter
	drop	1
	pushc	105
	call	_writeCharacter
	drop	1
	pushc	118
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	jmp	__35
__34:
	pushl	-3
	getf	4
	pushc	37
	eq
	brf	__36
	pushc	109
	call	_writeCharacter
	drop	1
	pushc	111
	call	_writeCharacter
	drop	1
	pushc	100
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
__36:
__35:
__33:
__31:
__29:
__27:
__25:
	rsf
	ret
