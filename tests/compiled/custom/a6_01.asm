//
// version
//
	.vers	6

//
// execution framework
//
__start:
	call	_main
	call	_exit
__stop:
	jmp	__stop

//
// Integer readInteger()
//
_readInteger:
	asf	0
	rdint
	popr
	rsf
	ret

//
// void writeInteger(Integer)
//
_writeInteger:
	asf	0
	pushl	-3
	wrint
	rsf
	ret

//
// Character readCharacter()
//
_readCharacter:
	asf	0
	rdchr
	popr
	rsf
	ret

//
// void writeCharacter(Character)
//
_writeCharacter:
	asf	0
	pushl	-3
	wrchr
	rsf
	ret

//
// Integer char2int(Character)
//
_char2int:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// Character int2char(Integer)
//
_int2char:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// void exit()
//
_exit:
	asf	0
	halt
	rsf
	ret

//
// void main()
//
_main:
	asf	0
	pushc	100
	call	_fac
	drop	1
	pushr
	call	_writeInteger
	drop	1
__0:
	rsf
	ret

//
// Integer fac(Integer)
//
_fac:
	asf	1
	pushc	1
	popl	0
	jmp	__3
__2:
	pushl	0
	pushl	-3
	mul
	popl	0
	pushl	-3
	pushc	1
	sub
	popl	-3
__3:
	pushl	-3
	pushc	1
	gt
	brt	__2
__4:
	pushl	0
	popr
	jmp	__1
__1:
	rsf
	ret
