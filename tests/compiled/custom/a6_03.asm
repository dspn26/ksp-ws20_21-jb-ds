//
// version
//
	.vers	6

//
// execution framework
//
__start:
	call	_main
	call	_exit
__stop:
	jmp	__stop

//
// Integer readInteger()
//
_readInteger:
	asf	0
	rdint
	popr
	rsf
	ret

//
// void writeInteger(Integer)
//
_writeInteger:
	asf	0
	pushl	-3
	wrint
	rsf
	ret

//
// Character readCharacter()
//
_readCharacter:
	asf	0
	rdchr
	popr
	rsf
	ret

//
// void writeCharacter(Character)
//
_writeCharacter:
	asf	0
	pushl	-3
	wrchr
	rsf
	ret

//
// Integer char2int(Character)
//
_char2int:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// Character int2char(Integer)
//
_int2char:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// void exit()
//
_exit:
	asf	0
	halt
	rsf
	ret

//
// void main()
//
_main:
	asf	0
	pushc	100
	call	_fracSum
	drop	1
	call	_writeAsDecimal
__0:
	rsf
	ret

//
// void fracSum(Integer)
//
_fracSum:
	asf	5
	pushc	1
	popg	0
	pushc	1
	popg	1
	pushc	1
	popl	0
	jmp	__3
__2:
	pushl	-3
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushc	1
	popl	0
	pushl	-3
	popl	1
	pushg	1
	pushl	1
	call	_lcm
	drop	2
	pushr
	popl	2
	pushl	2
	pushg	1
	div
	popl	4
	pushg	0
	pushl	4
	mul
	popg	0
	pushl	2
	popg	1
	pushl	2
	pushl	1
	div
	popl	4
	pushl	0
	pushl	4
	mul
	popl	0
	pushg	0
	pushl	0
	add
	popg	0
	pushg	0
	pushg	1
	call	_gcd
	drop	2
	pushr
	popl	3
	pushg	0
	pushl	3
	div
	popg	0
	pushg	1
	pushl	3
	div
	popg	1
	pushl	-3
	pushc	1
	sub
	popl	-3
__3:
	pushl	-3
	pushc	1
	gt
	brt	__2
__4:
__1:
	rsf
	ret

//
// Integer gcd(Integer, Integer)
//
_gcd:
	asf	1
	pushl	-4
	pushc	0
	eq
	brf	__6
	pushl	-3
	popr
	jmp	__5
__6:
	pushl	-3
	pushc	0
	eq
	brf	__7
	pushl	-4
	popr
	jmp	__5
__7:
__8:
	pushl	-4
	pushl	-3
	mod
	popl	0
	pushl	-3
	popl	-4
	pushl	0
	popl	-3
	pushl	-3
	pushc	0
	ne
	brt	__8
__9:
	pushl	-4
	popr
	jmp	__5
__5:
	rsf
	ret

//
// Integer lcm(Integer, Integer)
//
_lcm:
	asf	0
	pushl	-4
	pushl	-3
	mul
	pushl	-4
	pushl	-3
	call	_gcd
	drop	2
	pushr
	div
	popr
	jmp	__10
__10:
	rsf
	ret

//
// void writeAsDecimal()
//
_writeAsDecimal:
	asf	6
	pushg	0
	popl	0
	pushg	1
	popl	1
	pushl	0
	pushl	1
	div
	popl	2
	pushl	2
	call	_writeInteger
	drop	1
	pushc	46
	call	_writeCharacter
	drop	1
	pushl	0
	pushl	2
	pushl	1
	mul
	sub
	popl	0
	pushc	1
	popl	5
	jmp	__13
__12:
	pushl	0
	pushc	10
	mul
	popl	2
	pushl	2
	pushl	1
	div
	popl	3
	pushl	2
	pushl	3
	pushl	1
	mul
	sub
	popl	0
	pushl	5
	pushc	10
	eq
	dup
	brf	__16
	drop	1
	pushl	0
	pushc	10
	mul
	pushl	1
	div
	pushc	5
	ge
__16:
	brf	__15
	pushl	3
	pushc	1
	add
	popl	3
__15:
	pushl	3
	call	_writeInteger
	drop	1
	pushl	5
	pushc	1
	add
	popl	5
__13:
	pushl	5
	pushc	10
	le
	brt	__12
__14:
__11:
	rsf
	ret
