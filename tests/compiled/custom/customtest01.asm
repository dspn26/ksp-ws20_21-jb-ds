//
// version
//
	.vers	4

//
// execution framework
//
__start:
	call	_main
	call	_exit
__stop:
	jmp	__stop

//
// Integer readInteger()
//
_readInteger:
	asf	0
	rdint
	popr
	rsf
	ret

//
// void writeInteger(Integer)
//
_writeInteger:
	asf	0
	pushl	-3
	wrint
	rsf
	ret

//
// Character readCharacter()
//
_readCharacter:
	asf	0
	rdchr
	popr
	rsf
	ret

//
// void writeCharacter(Character)
//
_writeCharacter:
	asf	0
	pushl	-3
	wrchr
	rsf
	ret

//
// Integer char2int(Character)
//
_char2int:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// Character int2char(Integer)
//
_int2char:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// void exit()
//
_exit:
	asf	0
	halt
	rsf
	ret

//
// void main()
//
_main:
	asf	3
	call	_readInteger
	pushr
	popl	0
	call	_readInteger
	pushr
	popl	1
	call	_readInteger
	pushr
	popl	2
	pushl	0
	pushl	1
	pushl	2
	call	_func
	drop	3
	pushr
	call	_writeInteger
	drop	1
__0:
	rsf
	ret

//
// Integer func(Integer, Integer, Integer)
//
_func:
	asf	0
	pushl	-3
	call	_evaluateZ
	drop	1
	pushr
	pushl	-5
	pushl	-4
	call	_calculate
	drop	3
	pushr
	popr
	jmp	__1
__1:
	rsf
	ret

//
// Integer calculate(Boolean, Integer, Integer)
//
_calculate:
	asf	0
	pushl	-5
	brf	__3
	pushl	-4
	pushl	-3
	add
	popr
	jmp	__2
	jmp	__4
__3:
	pushl	-4
	pushl	-3
	sub
	popr
	jmp	__2
__4:
__2:
	rsf
	ret

//
// Boolean evaluateZ(Integer)
//
_evaluateZ:
	asf	0
	pushl	-3
	pushc	0
	ge
	popr
	jmp	__5
__5:
	rsf
	ret
