//
// version
//
	.vers	4

//
// execution framework
//
__start:
	call	_main
	call	_exit
__stop:
	jmp	__stop

//
// Integer readInteger()
//
_readInteger:
	asf	0
	rdint
	popr
	rsf
	ret

//
// void writeInteger(Integer)
//
_writeInteger:
	asf	0
	pushl	-3
	wrint
	rsf
	ret

//
// Character readCharacter()
//
_readCharacter:
	asf	0
	rdchr
	popr
	rsf
	ret

//
// void writeCharacter(Character)
//
_writeCharacter:
	asf	0
	pushl	-3
	wrchr
	rsf
	ret

//
// Integer char2int(Character)
//
_char2int:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// Character int2char(Integer)
//
_int2char:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// void exit()
//
_exit:
	asf	0
	halt
	rsf
	ret

//
// void main()
//
_main:
	asf	5
	call	_readInteger
	pushr
	popl	0
	call	_readInteger
	pushr
	popl	1
	call	_readInteger
	pushr
	popl	2
	pushl	0
	pushl	2
	call	_testUpper
	drop	2
	pushr
	dup
	brf	__1
	drop	1
	pushl	1
	pushl	2
	call	_testLower
	drop	2
	pushr
__1:
	popl	3
	pushl	3
	brf	__2
	pushc	1
	popl	4
	jmp	__3
__2:
	pushc	0
	popl	4
__3:
	pushl	4
	call	_writeInteger
	drop	1
__0:
	rsf
	ret

//
// Boolean testUpper(Integer, Integer)
//
_testUpper:
	asf	0
	pushl	-3
	pushl	-4
	lt
	popr
	jmp	__4
__4:
	rsf
	ret

//
// Boolean testLower(Integer, Integer)
//
_testLower:
	asf	0
	pushl	-3
	pushl	-4
	gt
	popr
	jmp	__5
__5:
	rsf
	ret
