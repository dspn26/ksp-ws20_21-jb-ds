//
// version
//
	.vers	6

//
// execution framework
//
__start:
	call	_main
	call	_exit
__stop:
	jmp	__stop

//
// Integer readInteger()
//
_readInteger:
	asf	0
	rdint
	popr
	rsf
	ret

//
// void writeInteger(Integer)
//
_writeInteger:
	asf	0
	pushl	-3
	wrint
	rsf
	ret

//
// Character readCharacter()
//
_readCharacter:
	asf	0
	rdchr
	popr
	rsf
	ret

//
// void writeCharacter(Character)
//
_writeCharacter:
	asf	0
	pushl	-3
	wrchr
	rsf
	ret

//
// Integer char2int(Character)
//
_char2int:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// Character int2char(Integer)
//
_int2char:
	asf	0
	pushl	-3
	popr
	rsf
	ret

//
// void exit()
//
_exit:
	asf	0
	halt
	rsf
	ret

//
// void main()
//
_main:
	asf	4
	call	_readInteger
	pushr
	popl	0
	pushl	0
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	0
	call	_int2char
	drop	1
	pushr
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	call	_readCharacter
	pushr
	popl	1
	pushl	1
	call	_char2int
	drop	1
	pushr
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	1
	call	_writeCharacter
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	call	_readInteger
	pushr
	popg	0
	call	_readInteger
	pushr
	popg	1
	pushg	0
	pushg	1
	add
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushg	0
	pushg	1
	sub
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushg	0
	pushg	1
	mul
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushg	0
	pushg	1
	div
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	pushg	0
	pushg	1
	mod
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	call	_readInteger
	pushr
	popg	0
	call	_readInteger
	pushr
	popl	0
	pushg	0
	pushl	0
	call	_returnFunction
	drop	2
	pushr
	popg	1
	pushg	1
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
__0:
	rsf
	ret

//
// Integer returnFunction(Integer, Integer)
//
_returnFunction:
	asf	1
	pushc	0
	popl	0
	pushl	-3
	pushl	-4
	eq
	brf	__2
	pushc	1
	call	_writeInteger
	drop	1
	jmp	__3
__2:
	pushc	0
	pushc	1
	sub
	call	_writeInteger
	drop	1
__3:
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	-3
	pushl	-4
	ne
	brf	__4
	pushc	1
	call	_writeInteger
	drop	1
	jmp	__5
__4:
	pushc	0
	pushc	1
	sub
	call	_writeInteger
	drop	1
__5:
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	-4
	pushl	-3
	lt
	brf	__6
	pushc	1
	call	_writeInteger
	drop	1
	jmp	__7
__6:
	pushc	0
	pushc	1
	sub
	call	_writeInteger
	drop	1
__7:
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	-4
	pushl	-3
	le
	brf	__8
	pushc	1
	call	_writeInteger
	drop	1
	pushc	10
	call	_writeCharacter
	drop	1
	jmp	__9
__8:
	pushc	0
	pushc	1
	sub
	call	_writeInteger
	drop	1
__9:
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	-4
	pushl	-3
	gt
	brf	__10
	pushc	1
	call	_writeInteger
	drop	1
	jmp	__11
__10:
	pushc	0
	pushc	1
	sub
	call	_writeInteger
	drop	1
__11:
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	-4
	pushl	-3
	ge
	brf	__12
	pushc	1
	call	_writeInteger
	drop	1
	jmp	__13
__12:
	pushc	0
	pushc	1
	sub
	call	_writeInteger
	drop	1
__13:
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	-4
	pushc	0
	gt
	dup
	brf	__16
	drop	1
	pushl	-3
	pushc	0
	gt
__16:
	brf	__14
	pushc	1
	call	_writeInteger
	drop	1
	jmp	__15
__14:
	pushc	0
	pushc	1
	sub
	call	_writeInteger
	drop	1
__15:
	pushc	10
	call	_writeCharacter
	drop	1
	pushl	-4
	pushc	0
	lt
	dup
	brt	__19
	drop	1
	pushl	-3
	pushc	0
	lt
__19:
	brf	__17
	pushc	1
	call	_writeInteger
	drop	1
	jmp	__18
__17:
	pushc	0
	pushc	1
	sub
	call	_writeInteger
	drop	1
__18:
	pushc	10
	call	_writeCharacter
	drop	1
	jmp	__21
__20:
	pushl	-4
	pushc	1
	add
	popl	-4
__21:
	pushl	-4
	pushl	-3
	lt
	brt	__20
__22:
	pushc	1
	popr
	jmp	__1
__1:
	rsf
	ret
